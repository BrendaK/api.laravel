<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\UserType;

class UserTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userTypes = [
        [
            "name" => "Admin",
        ],
        [
            "name" => "Consommateur",
        ],
    ];
    foreach($userTypes AS $userType):
        UserType::create($userType);
        endforeach;
    }
}
