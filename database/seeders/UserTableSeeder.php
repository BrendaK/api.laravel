<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\UserType;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
        [
            "lastname" => "Kong",
            "firstname" => "Brenda",
            "email" => "brendakong26@gmail.com",
            "password" => "azerty",
            "user_type_id" => "1",
        ],
        [
            "lastname" => "Lee",
            "firstname" => "Sarah",
            "email" => "sarahlee@gmail.com",
            "password" => "aaa",
        ],
        [
            "lastname" => "Toto",
            "firstname" => "Mike",
            "email" => "mike@gmail.com",
            "password" => "bbb",
        ],
        [
            "lastname" => "Titi",
            "firstname" => "Kikou",
            "email" => "kikou@gmail.com",
            "password" => "ccc",
        ],
        [
            "lastname" => "Loulou",
            "firstname" => "Bibou",
            "email" => "bibou@gmail.com",
            "password" => "ddd",
        ],
    ];
    foreach($users AS $user):
        User::create($user);
        endforeach;
    }
}
