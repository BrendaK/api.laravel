<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Models\User;

class UserController extends Controller
{
    public function getAll(){
        $users = User::all();

        return Response::json($users, 200);
    }

    public function getById($id){
        $user = User::find($id);

        return Response::json($user, 200);
    }

    public function getByUserTypeId($userTypeId){
        $users = User::where("user_type_id",$userTypeId)->get();

        return Response::json($users, 200);
    }

    public function create(Request $request){
        $user = new User;

        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        if ($request->user_type_id) $user->user_type_id = $request->user_type_id;
        $user->password = $request->password;

        $user->save();

        return Response::json($user, 200);
    }

    public function update($id, Request $request){
        $user = User::find($id);
        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        $user->user_type_id = $request->user_type_id;
        $user->password = $request->password;
        $user->save();

        return Response::json($user, 200);

    }

    public function delete($id){
        User::destroy($id);

        return Response::json("record delete", 200);
    }
}
