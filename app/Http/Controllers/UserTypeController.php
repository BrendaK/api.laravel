<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Models\UserType;


class UserTypeController extends Controller
{
    public function getAll(){
        $userType = UserType::all();

        return Response::json($userType, 200);
    }

    public function getById($id){
        $userType = UserType::find($id);

        return Response::json($userType, 200);
    }
}
