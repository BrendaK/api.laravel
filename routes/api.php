<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\UserController;
use App\Http\Controllers\UserTypeController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('users', [UserController::class, 'getAll']);
Route::get('users/{id}', [UserController::class, 'getById']);
Route::get('users/getByUserTypeId/{userTypeId}', [UserController::class, 'getByUserTypeId']);
Route::post('users/create', [UserController::class, 'create']);
Route::post('users/{id}', [UserController::class, 'update']);
Route::delete('users/{id}', [UserController::class, 'delete']);


Route::get('userTypes', [UserTypeController::class, 'getAll']);
Route::get('userTypes/{id}', [UserTypeController::class, 'getById']);


